/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import DashboardStack from './src/navigator/DashboardStack';
import AppStack from './src/navigator/AppStack';

const Tab = createBottomTabNavigator();

// import HomeScreen from './src/screens/HomeScreen';
// import BlankPage from './src/screens/BlankPage';

const App = () => {
  return (
    <>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <AppStack></AppStack>
      
    </>
  );
};

export default App;
