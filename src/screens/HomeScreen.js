import React from 'react';
import axios from 'axios';
import {StyleSheet, View, Text, FlatList, Image, Button, ScrollView} from 'react-native';
import PromotionItem from '../components/PromotionItem.component';
import JokesItem from '../components/JokesItem.component';
import WeatherItem from '../components/WeatherItem.component';


export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listPromotions: [],
      listJokes: [],
      selectedJokes: {
        id: '',
        jokes: ['', ''],
      },
      listWeather: [],
      selectedWeather: {
        location: null,
        short_note: null,
        suhu: null,
        kelembaban: null,
        angin: null,
        presipitasi: null,
        
      }
    };
  }

  componentDidMount() {
    // call axios
    // Promotions
    axios({
      method: 'GET',
      url: 'http://localhost:5000/promotions',
    })
      .then((res) => {
        this.setState({
          listPromotions: res.data,
        });
      })
      .catch((err) => {
        console.error(err);
      });

    // Jokes
    axios({
      method: 'GET',
      url: 'http://localhost:5000/jokes',
    })
      .then((res) => {
        this.setState({listJokes: res.data});
        this.timerJokes = setInterval(() => {
          const randomNumber = Math.floor(
            Math.random() * this.state.listJokes.length,
          );
          this.setState({selectedJokes: this.state.listJokes[randomNumber]});
        }, 5000);
      })
      .catch((err) => {
        console.error(err);
      });

// Weather
  //   axios({
  //     method: 'GET',
  //     url: 'http://localhost:5000/weather'
  // })
  // .then((res) => {
  //     console.info('data length ', res.data.length);
  //     this.setState({ listWeather: res.data, selectedWeather: res.data[0] }, () => {
  //         setInterval(() => {
  //             if (this.state.selectedWeather !== {}){ // Why if else here? => to ensure the selected object is defined
  //                 const index = this.state.listWeather.findIndex(result => result.location === this.state.selectedWeather.location) // Why findIndex? => to define the object order within the array (el => bebas bisa diganti)
  //                 // index === this.state.listWeather.length - 1 ? 0 : index + 1 { (if statement) ? true : false }
  //                 this.setState({ selectedWeather: this.state.listWeather[index === this.state.listWeather.length - 1 ? 0 : index + 1]}) // How to read ternary => refers to line 26
  //             }
  //         }, 5000)
  //     })
  // })
  // .catch((err) => {
  //     //error
  //     console.error('Error nih')
  // })
  axios({
    method: 'GET',
    url: 'http://localhost:5000/weather',
  })
    .then((res) => {
      this.setState({listWeather: res.data});
      this.timerWeather = setInterval(() => {
        const randomNumber = Math.floor(
          Math.random() * this.state.listWeather.length,
        );
        this.setState({selectedWeather: this.state.listWeather[randomNumber]});
      }, 5000);
    })
    .catch((err) => {
      console.error(err);
    });  
  }

  componentWillUnmount() {
    clearInterval(this.timerJokes);
    clearInterval(this.timerWeather)
  }

  render() {
    return (
      <ScrollView>
        
        <View style={styles.header}>
          <View style={styles.headerImage}>
            <Image
              source={{
                uri:
                  'https://cdn3.iconfinder.com/data/icons/generic-avatars/128/avatar_portrait_woman_female_afro_1-256.png',
              }}
              style={styles.profilImage}
            />
          </View>
          <View style={styles.headerTitle}>
            <Text style={styles.titleText}>Hi, Lidya</Text>
          </View>
        </View>
        <View style={styles.body}>
          {/* Carousel */}
          <Button color='#444' onPress={() => this.props.navigation.navigate('Other')} title='Go to other'></Button>
          <FlatList
            pagingEnabled={true}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={this.state.listPromotions}
            renderItem={({item}) => (
              <PromotionItem url={item.url} image={item.preview_image} />
            )}
            keyExtractor={(item) => String(item.id)}
          />
          {/* Jokes */}
          <View style={styles.itemJokes}>
            <View style={styles.jokesHeader}>
              <Text>Jokes For You</Text>
              <Text>See All</Text>
            </View>
            <View>
              <JokesItem
                pertanyaan={this.state.selectedJokes.jokes[0]}
                jawaban={this.state.selectedJokes.jokes[1]}
              />
            </View>
          </View>
          {/* Weather */}
          <View style={styles.itemWeather}>
          <View style={styles.weatherHeader}>
            <Text>Weather Condition</Text>
              <Text>See All</Text>
          </View>
          <View>
            <WeatherItem 
              cuaca={this.state.selectedWeather} />
          </View>
        </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: 'white',
  },
  headerImage: {
    marginHorizontal: 10,
  },
  profilImage: {
    width: 30,
    height: 30,
  },
  headerTitle: {
    justifyContent: 'center',
  },
  titleText: {
    fontFamily: 'segoe-ui',
    fontWeight: 'bold',
    color: '#333333',
    fontSize: 15,
  },
  body: {
    padding: 10,
    backgroundColor: '#FFFFFF',
  },
  itemJokes: {
    marginVertical: 10,
  },
  jokesHeader: {
    marginHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemWeather: {
    marginVertical: 10,
  },
  weatherHeader: {
    marginHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
