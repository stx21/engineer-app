import React from 'react';
import {Text, View, Button} from 'react-native';

export default function LoginScreen(props) {
    return (
        <View>
            <Text> AYO LOGIN</Text>
            <Button 
            title='Go to'
            color='blue'
            onPress={() => props.navigation.navigate('Main')}> </Button>
        </View>
    );
}