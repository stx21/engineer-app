import React from 'react';
// import axios from 'axios';
import {View, Text, StyleSheet} from 'react-native';

export default class WeatherItem extends React.Component{
    constructor(props){
        super(props);
        // this.state = {
        //     listWeather: [],
        //     selectedWeather: {}
        // }
    }

    // componentDidMount(){
    //     axios({
    //         method: 'GET',
    //         url: 'http://localhost:5000/weather'
    //     })
    //     .then((res) => {
    //         console.info('data length ', res.data.length);
    //         this.setState({ listWeather: res.data, selectedWeather: res.data[0] }, () => {
    //             setInterval(() => {
    //                 if (this.state.selectedWeather !== {}){ // Why if else here? => to ensure the selected object is defined
    //                     const index = this.state.listWeather.findIndex(result => result.location === this.state.selectedWeather.location) // Why findIndex? => to define the object order within the array (el => bebas bisa diganti)
    //                     // index === this.state.listWeather.length - 1 ? 0 : index + 1 { (if statement) ? true : false }
    //                     this.setState({ selectedWeather: this.state.listWeather[index === this.state.listWeather.length - 1 ? 0 : index + 1]}) // How to read ternary => refers to line 26
    //                 }
    //             }, 5000)
    //         })
    //     })
    //     .catch((err) => {
    //         //error
    //         console.error('Error nih')
    //     })
    

    render() {
        return (
          <View style={styles.itemContainer}>
            <View style={styles.itemWeather}>
                <Text> {this.props.cuaca.location}</Text>
                <Text> {this.props.cuaca.short_note}</Text>
                <Text> {this.props.cuaca.suhu}</Text>
                <Text> {this.props.cuaca.kelembaban}</Text>
                <Text> {this.props.cuaca.angin}</Text>
                <Text> {this.props.cuaca.presipitasi}</Text>
            </View>
            {/* {Object.keys(this.props.cuaca).map(keyItem => (
            <Text>{keyItem}: {this.props.cuaca[keyItem]}</Text>))} */}
          </View>
        );
        
      }
    }

    const styles = StyleSheet.create({
        itemContainer: {
          backgroundColor: 'white',
          borderRadius: 15,
          margin: 10,
          padding: 20,
          alignItems: 'stretch',
          height: 150,
          elevation: 10,
          shadowColor: '#000',
          shadowOffset: {width: 0, height: 3},
          shadowOpacity: 0.5,
          shadowRadius: 15,
        },
        itemPertanyaan: {
          fontFamily: 'sego-ui',
          fontSize: 18,
          fontWeight: 'bold',
        },
        itemJawaban: {
          fontFamily: 'sego-ui',
          fontSize: 18,
          fontStyle: 'italic',
        },
      });