// import React from 'react';
// import {View, Text, StyleSheet} from 'react-native';

// export default class JokesItem extends React.Component {
//   constructor(props) {
//     super(props);
//   }

//   render() {
//     return (
//         <View style={styles.itemContainer}>  

//           {/* <View style={styles.itemContent}>
//             <Text>
//               {this.props.pertanyaan}
//             </Text>
//           </View>

//           <View style={styles.itemContent}>
//             <Text>
//               {this.props.jawaban}
//             </Text>
//           </View> */}

//           {
//               this.props.jokes.map((item, index) => ( //
//                 <View key={index} style={styles.itemContent}>
//                 <Text>
//                   {item}
//                 </Text>
//               </View>
//               ))
//           }
//         </View>
      
//     );
//   }
// }

// const styles = StyleSheet.create({
//     itemContainer: {
//         display: 'flex',    //properti:value 'objek'
//         padding: 10,
//         width: 400,
        
//       },

//       itemContent: {
//         // textAlign: 'justify',
//         // backgroundColor: 'darksalmon',
//         // padding: 5,
//         // margin: 10,
//         // height: 70,
    
//       },
//       title: {
//         fontWeight: 'bold',
//       },
// });


// // import React, { Component } from 'react';
// // import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';

// // export default class JokesItem extends Component {
// //   render() {
// //     return (
// //       <Container>
// //         <Header />
// //         <Content>
// //           <Card>
// //             <CardItem>
// //               <Body>
// //                 <Text>
// //                 {this.props.jokes}
// //                 </Text>
// //               </Body>
// //             </CardItem>
// //           </Card>
// //         </Content>
// //       </Container>
// //     );
// //   }
// // }

// ----------
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class JokesItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.itemContainer}>
        <Text style={styles.itemPertanyaan}>{this.props.pertanyaan}</Text>
        <Text style={styles.itemJawaban}>{this.props.jawaban}</Text>
      </View>
    );
    
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: 'white',
    borderRadius: 15,
    margin: 10,
    padding: 20,
    alignItems: 'stretch',
    height: 130,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 15,
  },
  itemPertanyaan: {
    fontFamily: 'sego-ui',
    fontSize: 18,
    fontWeight: 'bold',
  },
  itemJawaban: {
    fontFamily: 'sego-ui',
    fontSize: 18,
    fontStyle: 'italic',
  },
});
