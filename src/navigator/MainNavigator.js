import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();

import BlankPage from '../screens/BlankPage';
import HomeScreen from '../screens/HomeScreen';
import DashboardStack from './DashboardStack';

export default function MainNavigator() {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Dashboard" component={DashboardStack} />
            <Tab.Screen name="HomeScreen" component={HomeScreen} />
            <Tab.Screen name="BlankPage" component={BlankPage} />
        </Tab.Navigator>
    );
}